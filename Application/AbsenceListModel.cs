﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace absence_list_api.Application
{
    public class AbsenceListModel
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
