﻿using System.Collections.Generic;
using System.Linq;
using absence_list_api.Database;

namespace absence_list_api.Application
{
    public interface IAbsenceListService
    {
        IEnumerable<AbsenceListModel> GetAll();
    }

    public class AbsenceListService : IAbsenceListService
    {
        private readonly IAbsenceListRepository _absenceListRepository;

        public AbsenceListService(IAbsenceListRepository absenceListRepository)
        {
            _absenceListRepository = absenceListRepository;
        }

        public IEnumerable<AbsenceListModel> GetAll()
        {
            var entities = _absenceListRepository.GetAll(0);
            var models = entities.Select(entity => new AbsenceListModel
            {
                DateFrom = entity.DateFrom,
                DateTo = entity.DateTo
            });
            return models;
        }
    }
}
