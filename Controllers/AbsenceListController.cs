﻿using System.Collections.Generic;
using absence_list_api.Database;
using Microsoft.AspNetCore.Mvc;

namespace absence_list_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AbsenceListController : ControllerBase
    {
        private readonly IAbsenceListRepository _absenceListRepository;

        public AbsenceListController(IAbsenceListRepository absenceListRepository)
        {
            _absenceListRepository = absenceListRepository;
        }

        [Route("{index}")]
        public IEnumerable<AbsenceListItemEntity> GetAll(int index)
        {
            return _absenceListRepository.GetAll(index);
        }
    }
}
