﻿using absence_list_api.Database;
using Microsoft.AspNetCore.Mvc;

namespace absence_list_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeedController : ControllerBase
    {
        private readonly IAbsenceListRepository _absenceListRepository;

        public SeedController(IAbsenceListRepository absenceListRepository)
        {
            _absenceListRepository = absenceListRepository;
        }

        [HttpGet]
        public string Get()
        {
            _absenceListRepository.Seed();
            return "Done!";
        }
    }
}
