﻿using Microsoft.EntityFrameworkCore;

namespace absence_list_api.Database
{
    public class AbsenceListContext : DbContext
    {
        public DbSet<AbsenceListItemEntity> AbsenceListItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=AbsenceList.db");
        }
    }
}
