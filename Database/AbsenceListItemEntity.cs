﻿namespace absence_list_api.Database
{
    public class AbsenceListItemEntity
    {
        public int Id { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
