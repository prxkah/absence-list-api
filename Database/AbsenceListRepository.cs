﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace absence_list_api.Database
{
    public interface IAbsenceListRepository
    {
        IEnumerable<AbsenceListItemEntity> GetAll(int index);
        void Reset();
        void Seed();
    }

    public class AbsenceListRepository : IAbsenceListRepository
    {
        public IEnumerable<AbsenceListItemEntity> GetAll(int index)
        {
            using (var context = new AbsenceListContext())
            {
                return context.AbsenceListItems
                    .OrderByDescending(x => x.DateFrom)
                    .Skip(index == 0 ? 0 : index - 1)
                    .Take(index == 0 ? 5 : 5 + 1)
                    .ToList();
            }
        }

        public void Reset()
        {
            using (var context = new AbsenceListContext())
            {
                var entities = context.AbsenceListItems.ToList();
                context.RemoveRange(entities);
                context.SaveChanges();
            }
        }

        public void Seed()
        {
            var dateFrom = new DateTime(2019, 01, 01);
            var dateTo = new DateTime(2019, 12, 31);
            var entities = new List<AbsenceListItemEntity>();

            for (var date = dateFrom; date <= dateTo; date = date.AddDays(1))
            {
                entities.Add(new AbsenceListItemEntity
                {
                    DateFrom = date.ToShortDateString(),
                    DateTo = date.ToShortDateString()
                });
            }

            using (var context = new AbsenceListContext())
            {
                context.AddRange(entities);
                context.SaveChanges();
            }
        }
    }
}
